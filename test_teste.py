import requests
import pytest
import json
import datetime


class Testes:
    def test_adicionar_viagens(self):
        andress = "http://127.0.0.1:5000/viagens"
        request_json = {
            'horaPartida': '15:00',
            'horaChegada': '19:00',
            'cidadePartida': 'Piratini',
            'cidadeChegada': 'Pelotas',
            'valor': 8,
            'tipoViagem': 'Estudante'
        }
        response = requests.get(andress, request_json)
        assert ((response.status_code == 200) and (request_json['valor'] >= 0) and (
            type(request_json['valor']) != type("a")) and type(request_json['cidadePartida']) != type(1))
